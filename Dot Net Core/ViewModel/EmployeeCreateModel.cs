﻿using Dot_Net_Core.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.ViewModel
{
    public class EmployeeCreateModel
    {
        [Required]
        [MaxLength(20)]
        public string Name { get; set; }

        [Required]
        [RegularExpression(@"^((?!\.)[\w-_.]*[^.])(@\w+)(\.\w+(\.\w+)?[^.\W])$", ErrorMessage = "Please Enter Correct Email Address")]
        public string Email { get; set; }

        [Required]
        public Dept? Department { get; set; }


        public IFormFile Photo { get; set; }


    }
}
