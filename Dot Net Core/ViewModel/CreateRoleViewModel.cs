﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.ViewModel
{
    public class CreateRoleViewModel
    {

        [Required]
        [Display(Name ="Role")]
        public string RoleName { get; set; }
    }
}
