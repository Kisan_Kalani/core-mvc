﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.ViewModel
{
    public class EmployeeEditModel :EmployeeCreateModel
    {
        public int Id { get; set; }

        public string ExistingPhoto { get; set; }
    }
}
