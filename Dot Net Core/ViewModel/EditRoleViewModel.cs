﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.ViewModel
{
    public class EditRoleViewModel
    {
        public EditRoleViewModel()
        {
            Users = new List<string>();
        }


        [Required(ErrorMessage ="Id is Required a")]
        public string Id { get; set; }

        public string RoleName { get; set; }

        public List<string> Users { set; get; }

    }
}
