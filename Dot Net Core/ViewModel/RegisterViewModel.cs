﻿using Dot_Net_Core.Utilities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace Dot_Net_Core.ViewModel
{
    public class RegisterViewModel
    {

        [Required]
        [DataType(DataType.EmailAddress)]
        [Remote(action: "IsEmailInUse", controller:"Account")]
        [CustomValidate(allowedDomain:"kalani.com",ErrorMessage ="email domain mus be kalani.com")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public  string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name ="Confirm Password")]
        [Compare("Password",ErrorMessage ="Password and Confirm Password Don't Match.")]
        public string ConfirmPassword { get; set; }

        
        public string City{ get; set; }


    }
}
