﻿using Dot_Net_Core.Models;
using Dot_Net_Core.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _IEmployeeRepository;
        
        private readonly IHostingEnvironment hostingEnvironment;

        public HomeController(IEmployeeRepository iemployeeRepository, IHostingEnvironment hostingEnvironment)
        {
            _IEmployeeRepository = iemployeeRepository;
            this.hostingEnvironment = hostingEnvironment;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            var emp = _IEmployeeRepository.GetAllEmployee();
            return View(emp);

        }

        public IActionResult Details(int? id)
        {
            Employee employee = _IEmployeeRepository.GetDetails(id.Value);

            if(employee == null)
            {
                Response.StatusCode = 404;
                return View("ErrorEmployee404",id.Value);
            }


            ViewModelDetails viewModelDetails = new ViewModelDetails()
            {
                employee = employee,
                PageTitle = "Details"
            };

            // Employee model = _IEmployeeRepository.GetEmployee(1);
            //ViewData["Title"] = "Details";
            return View(viewModelDetails);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateModel empPhotoModel)
        {

            if (ModelState.IsValid)
            {
                string uniqueFileName = null;


                if (empPhotoModel.Photo != null)
                {
                    uniqueFileName = ProcessUploading(empPhotoModel);
                }

                Employee newEmployee = new Employee()
                {
                    Name = empPhotoModel.Name,
                    Email = empPhotoModel.Email,
                    Department = empPhotoModel.Department,
                    PhotoPath = uniqueFileName
                };

                _IEmployeeRepository.Add(newEmployee);
                return RedirectToAction("Details", new { Id = newEmployee.Id });
            }

            return View();

        }


        [HttpGet]
        public IActionResult Edit(int Id)
        {
            Employee employee = _IEmployeeRepository.GetDetails(Id);

            EmployeeEditModel employeeEditModel = new EmployeeEditModel()
            {
                Id = employee.Id,
                Name = employee.Name,
                Email = employee.Email,
                Department = employee.Department,
                ExistingPhoto = employee.PhotoPath
            };

            return View(employeeEditModel);
        }

        [HttpPost]
        public IActionResult Edit(EmployeeEditModel employeeEditModel)
        {
            if (ModelState.IsValid)
            {
                Employee employee = _IEmployeeRepository.GetDetails(employeeEditModel.Id);

                employee.Name = employeeEditModel.Name;
                employee.Email = employeeEditModel.Email;
                employee.Department = employeeEditModel.Department;
                employee.PhotoPath = ProcessUploading(employeeEditModel);

                if (employeeEditModel.Photo != null)
                {
                    if (employeeEditModel.ExistingPhoto != null)
                    {
                        string filepath = Path.Combine(hostingEnvironment.WebRootPath, "Images", employeeEditModel.ExistingPhoto);
                        System.IO.File.Delete(filepath);
                    }

                    employee.PhotoPath = ProcessUploading(employeeEditModel);
                }

                Employee emp = _IEmployeeRepository.Update(employee);
                return RedirectToAction("Index");
            }
            return View();
        }


        private string ProcessUploading(EmployeeCreateModel model)
        {
            string uniqueFileName = null;
            if (model.Photo != null)
            {

                string uploadFolder = Path.Combine(hostingEnvironment.WebRootPath, "Images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                string filepath = Path.Combine(uploadFolder, uniqueFileName);

                using (var filePath = new FileStream(filepath, FileMode.Create))
                {
                    model.Photo.CopyTo(filePath);
                }
            }
            return uniqueFileName;
        }
    }
}
