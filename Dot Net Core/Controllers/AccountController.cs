﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dot_Net_Core.Models;
using Dot_Net_Core.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace Dot_Net_Core.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> usermanager;
        private readonly SignInManager<ApplicationUser> signinmanager;

        public AccountController(UserManager<ApplicationUser> usermanager, SignInManager<ApplicationUser> signinmanager)
        {
            this.usermanager = usermanager;
            this.signinmanager = signinmanager;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {

            return View();
        }




        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    City = model.City
                };

                var result = await usermanager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    if (signinmanager.IsSignedIn(User) && User.IsInRole("Admin"))
                    {
                        return RedirectToAction("ListUsers","Administration");
                    }
                    else
                    {
                        await signinmanager.SignInAsync(user, isPersistent: false);
                        return RedirectToAction("Index", "Home");

                    }

                }

                foreach (var errors in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, errors.Description);

                }
            }


            return View();
        }


        [AllowAnonymous]
        [AcceptVerbs("Get", "Post")]

        public async Task<IActionResult> IsEmailInUse(string email)
        {
            var User = await usermanager.FindByEmailAsync(email);

            if (User == null)
            {
                return Json(true);
            }
            else
            {
                return Json($"*Email {email} is aslready taken.Kindly use another Email*");
            }


        }


        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Logout()
        {
            await signinmanager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = "")
        {
            if (ModelState.IsValid)
            {
                var Result = await signinmanager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, false);

                if (Result.Succeeded)
                {
                    if (!String.IsNullOrEmpty(returnUrl) && Url.IsLocalUrl(returnUrl))
                    {
                        return LocalRedirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Invalid Login Attempt");
                }
            }

            return View(model);
        }








    }
}