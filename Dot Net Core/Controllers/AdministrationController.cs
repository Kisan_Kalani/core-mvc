﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Dot_Net_Core.Models;
using Dot_Net_Core.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace Dot_Net_Core.Controllers
{
    [Authorize(Roles ="Admin")]
    public class AdministrationController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> usermanager;

        public AdministrationController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> usermanager)
        {
            this.roleManager = roleManager;
            this.usermanager = usermanager;
        }

        [HttpGet]
        public IActionResult ListUsers()
        {
            var users =usermanager.Users;
            return View(users);
        }


        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateRole(CreateRoleViewModel model)
        {

            if (ModelState.IsValid)
            {
                IdentityRole identityRole = new IdentityRole()
                {
                    Name = model.RoleName
                };

                IdentityResult result = await roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return RedirectToAction("ListRole", "Administration");
                }

                foreach (IdentityError errors in result.Errors)
                {
                    ModelState.AddModelError("", errors.Description);
                }


            }
            return View(model);
        }


        [HttpGet]
        public IActionResult ListRole()
        {
            var role = roleManager.Roles;

            return View(role);
        }


        [HttpGet]
        public async Task<IActionResult> EditRole(string Id)
        {
            if (ModelState.IsValid)
            {
                var role = await roleManager.FindByIdAsync(Id);

                if (role == null)
                {
                    ViewBag.ErrorMessage = $"Role with Id {Id} not Found";
                    return View("ErrorEmployee404");
                }

                var model = new EditRoleViewModel()
                {
                    Id = role.Id,
                    RoleName = role.Name,

                };

                foreach (var user in usermanager.Users.ToList())
                {
                    if (await usermanager.IsInRoleAsync(user, role.Name))
                    {
                        model.Users.Add(user.UserName);
                    }

                }

                return View(model);

            }
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> EditRole(EditRoleViewModel model)
        {
            var role = await roleManager.FindByIdAsync(model.Id);

            if (role == null)
            {
                ViewBag.ErrorMessage = $"Role with ID {model.Id} not found";
                return View("ErrorEmployee404");
            }
            else
            {
                role.Name = model.RoleName;

                var result = await roleManager.UpdateAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction("ListRole");
                }

                foreach (var error in result.Errors)
                {

                    ModelState.AddModelError("", error.Description);
                }

                return View(model);
            }

        }



        [HttpGet]
        public async Task<IActionResult> UsersInRole(string roleId)
        {
            ViewBag.roleId = roleId;

            var role = await roleManager.FindByIdAsync(roleId);

            if (role == null)
            {
                ViewBag.ErrorMessage = $"role with ID {roleId} not found";
                return View("ErrorEmployee404");
            }

            var model = new List<UsersInRoleViewModel>();

            foreach(var user in usermanager.Users.ToList())
            {

                UsersInRoleViewModel usersInRole = new UsersInRoleViewModel()
                {
                    UserId = user.Id,
                    UserName=user.UserName
                
                };


                if (await usermanager.IsInRoleAsync(user, role.Name))
                {
                    usersInRole.IsSelected = true;
                }
                else
                {
                    usersInRole.IsSelected = false;
                }

                model.Add(usersInRole);
            }


            return View(model);
        }


        [HttpPost]
        public async Task<IActionResult> UsersInRole(List<UsersInRoleViewModel> model, string Id)
        {
             var role =await roleManager.FindByIdAsync(Id);

            if (role == null)
            {
                ViewBag.ErrorMessage = $"Role withId {Id} Not Found";
                return View();
            }

            for(int i=0;i<model.Count(); i++)
            {
                var user  = await usermanager.FindByIdAsync(model[i].UserId);

                IdentityResult result = null;

                if (model[i].IsSelected &&  !(await usermanager.IsInRoleAsync(user, role.Name)))
                {
                     result = await usermanager.AddToRoleAsync(user, role.Name);
                }
                else if (!(model[i].IsSelected) && (await usermanager.IsInRoleAsync(user,role.Name)))
                {
                    result = await usermanager.RemoveFromRoleAsync(user, role.Name);      
                }
                else
                {
                    continue;
                }

                if (result.Succeeded)
                {
                    if (i < model.Count() - 1)
                    {
                        continue;
                    }
                    else
                    {
                        return RedirectToAction("EditRole", new { Id = Id });
                    }
                }

            }

            return RedirectToAction("EditRole", new { Id = Id });

        }


        [HttpGet]
        public async Task<IActionResult> EditUsers(string Id)
        {
             var user = await usermanager.FindByIdAsync(Id);

            if (user == null)
            {
                return View("ErrorEmployee404");
            }

            var userClaims = await usermanager.GetClaimsAsync(user);
            var userRoles = await usermanager.GetRolesAsync(user);

            var model = new EditUserViewModel()
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.UserName,
                City = user.City,
                Claims = userClaims.Select(c => c.Value).ToList(),
                Roles = userRoles.ToList()
            };

            return View(model);
        }


        
        [HttpPost]
        public async Task<IActionResult> EditUsers(EditUserViewModel model)
        {
           var  user =  await usermanager.FindByIdAsync(model.Id);


            if (user == null)
            {
                return View("ErrorEmployee404");
            }
            else
            {
                user.UserName = model.UserName;
                user.Email = model.Email;
                user.City = model.City;

             var result=await usermanager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return RedirectToAction("ListUsers");
            }
            
            foreach(var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }

            return View(model);

            }

        }
         


        [HttpPost]
        public async Task<IActionResult> DeleteUser(string Id)
        {
            var user  =await usermanager.FindByIdAsync(Id);

            if (user == null)
            {
                return View("ErrorEmployee404");
            }
            else
            {
                var result=await usermanager.DeleteAsync(user);

                if (result.Succeeded)
                {
                    return RedirectToAction("ListUsers");
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("",error.Description);
                }


                return View("ListUsers");
            }


        }



        [HttpPost]
        public async Task<IActionResult> DeleteRole(string Id)
        {
           var role=await roleManager.FindByIdAsync(Id);

            if (role == null)
            {
                return View("ErrorEmployee404");
            }
            else
            {
               var result=await roleManager.DeleteAsync(role);

                if (result.Succeeded)
                {
                    return RedirectToAction("ListRole");
                }

                foreach(var error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }

                return View("ListRole");
            }
        }
    }
}

