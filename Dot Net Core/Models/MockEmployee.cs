﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.Models
{
    public class MockEmployee : IEmployeeRepository
    {

        private readonly List<Employee> _empList;
        public MockEmployee()
        {
            _empList = new List<Employee>()
            {
                new Employee(){Id=1,Name="Kisan Kalani",Email="Kalanik002@gmail.com",Department=Dept.IT},
                new Employee(){Id=2,Name="Bhavesh Kalani",Email="Bhaveshrk922@gmail.com",Department=Dept.Sales},
                new Employee(){Id=3,Name="Mohit Kalani",Email="Mohitrk444430@gmail.com",Department=Dept.HR}
            };
        }

        public Employee Add(Employee employee)
        {
            employee.Id = _empList.Max(e => e.Id) + 1;
            _empList.Add(employee);
            return employee;
        }

        public Employee Delete(int Id)
        {
            Employee employee =_empList.FirstOrDefault(e => e.Id == Id);

            if (employee != null)
            {
                _empList.Remove(employee);
            }
            return employee;
        
        }

        public IEnumerable<Employee> GetAllEmployee()
        {
            return _empList;
        }

        public Employee GetDetails(int Id)
        {
            Employee employee=_empList.FirstOrDefault(e => e.Id == Id);
            return employee;
        }

        public Employee Update(Employee employee)
        {

           Employee empChanges= _empList.FirstOrDefault(e => e.Id == employee.Id);
            if (empChanges != null)
            {
                empChanges.Name = employee.Name;
                empChanges.Email = employee.Email;
                empChanges.Department = employee.Department;
            }
            return empChanges;
        }
    }
}
