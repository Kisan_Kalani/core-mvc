﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.Models
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> GetAllEmployee();

        Employee Add(Employee empAdd);

        Employee GetDetails(int Id);

        Employee Delete(int Id);

        Employee Update(Employee employee);

    }
}
