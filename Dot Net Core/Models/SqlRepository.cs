﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Dot_Net_Core.Models
{
    public class SqlRepository : IEmployeeRepository
    {
        private readonly AppDBContext context;

        public SqlRepository(AppDBContext context)
        {
            this.context = context;
        }

        public Employee Add(Employee empAdd)
        {
            context.Employees.Add(empAdd);
            context.SaveChanges();
            return empAdd;
        }

        public Employee Delete(int Id)
        {
            Employee employee =context.Employees.Find(Id);

            if (employee != null)
            {
                context.Remove(employee);
                context.SaveChanges();
            }
            return employee;


        }

        public IEnumerable<Employee> GetAllEmployee()
        {
            return context.Employees.ToList();
        }

        public Employee GetDetails(int Id)
        {
            Employee emp= context.Employees.Find(Id);
            return emp;
        }

        public Employee Update(Employee employee)
        {
            var emp = context.Employees.Attach(employee);
            emp.State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            context.SaveChanges();
            return employee;

        }
    }
}
